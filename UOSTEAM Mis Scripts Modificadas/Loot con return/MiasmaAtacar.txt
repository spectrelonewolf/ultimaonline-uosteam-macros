@createlist 'unstuck'
if list 'unstuck' == 0
  pushlist 'unstuck' 'east'
  pushlist 'unstuck' 'west'
  pushlist 'unstuck' 'south'
  pushlist 'unstuck' 'north'
endif
//////////////////////////////////////////////////////////////////
@getenemy 'enemy' 'criminal' 'gray' 'closest'
if @graphic 'enemy' == 0x30 and @hits 'enemy' > 0
  getenemy 'found'
  @settimer 'pathfind' 0
  while @graphic 'enemy' == 0x30 and not @inrange 'enemy' 0
    if dead
      break
    elseif @timer 'pathfind' >= 5000
      // Diverge
      if direction == 0
        @poplist 'unstuck' 'north'
      elseif direction == 2
        @poplist 'unstuck' 'east'
      elseif direction == 4
        @poplist 'unstuck' 'south'
      elseif direction == 6
        @poplist 'unstuck' 'west'
      endif
      // Unstuck
      if list 'unstuck' != 0
        for 5
          run 'unstuck[0]'
          pause 100
        endfor
        poplist 'unstuck' 'front'
      endif
      break
    elseif @x 'enemy' > x 'self' and @y 'enemy' > y 'self'
      run 'southeast'
    elseif @x 'enemy' < x 'self' and @y 'enemy' > y 'self'
      run 'southwest'
    elseif @x 'enemy' > x 'self' and @y 'enemy' < y 'self'
      run 'northeast'
    elseif @x 'enemy' < x 'self' and @y 'enemy' < y 'self'
      run 'northwest'
    elseif @x 'enemy' > x 'self' and @y 'enemy' == y 'self'
      walk 'east'
    elseif @x 'enemy' < x 'self' and @y 'enemy' == y 'self'
      walk 'west'
    elseif @x 'enemy' == x 'self' and @y 'enemy' > y 'self'
      walk 'south'
    elseif @x 'enemy' == x 'self' and @y 'enemy' < y 'self'
      walk 'north'
    endif
    pause 100
  endwhile
  while @graphic 'enemy' == 0x30 and @inrange 'enemy' 2
    if @graphic 'enemy' == 0x30 and @hits 'enemy' == maxhits 'enemy'
      virtue 'Honor'
      waitfortarget 5000
      target! 'last'
    endif
    attack 'enemy'
    clearjournal
    if @inrange 'enemy' 6
      if not buffexists '1082'
        cast 'consecrate weapon'
      endif
    endif
    if not buffexists 'enemy of one'
      cast 'enemy of one'
    endif
    if not buffexists 'Counter Attack' and mana >= 10
      cast 'Counter Attack'
    endif
    if stam < 80 and mana > 15
      cast 'divine fury'
    endif
    if stam < 45
      useobject 0x41b4e991
    endif
    if hits < 35
      useobject 0x409a3db4
    endif
    if not buffexists 'lightning strike'
      cast 'lightning strike'
      pause 500
    endif
    if hits <= 95
      cast "confidence"
    endif
    if weight 'self' > 500
      usetype 0xe76
      waitfortarget 1000
      targettype 0xeed
      pause 1000
      if injournal 'This item is out of charges.' 'system'
        usetype 0x26b8
        waitfortarget 1500
        targettype 0xe76
        clearjournal
        pause 550
        canceltarget
      endif
    endif
  endwhile
  pause 3500
else
  if poisoned 'self'
    cast "Cleanse by Fire"
    waitfortarget 3000
    target! 'self'
  else
    //dress 'vestir'
    if  str < 120
      // or dex < 150
      cast "Remove Curse"
      pause 2500
      target! 'self'
    else
      if hits < 80
        if not buffexists 'Confidence'
          cast 'Confidence'
        endif
        // endif
        if gold >= 45000
          if @findtype '0xe76' 'any' 'backpack'
            @setalias 'BagOfSending' 'found'
            if @findtype '0xeed' 'any' 'backpack'
              useobject 'BagOfSending'
              waitfortarget 400
              target 'found' 400
            endif
          endif
        else
          if stam > 1
            target! 0x44199817
            pause 400
          endif
        endif
      endif
    endif
  endif
endif
