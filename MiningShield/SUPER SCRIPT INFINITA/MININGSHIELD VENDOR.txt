//AGENT BUY: INGOTS 360(son 20 escudos),martillo
//AGENT SELL: VENDO ESCUDOS y MARtillo
sysmsg '[ MININGSHIELD VENDOR ]' 80
sysmsg '[ -------------- ]' 80
//selecciono caja de guardado
if not @findalias 'box5'
  useobject 'bankbook'
  waitforgump 0x554b87f3 5000
  replygump 0x554b87f3 5
  pause 2000
  msg 'bank'
  pause 1000
  headmsg 'Donde guardas todo?'
  sysmsg '[ Donde guardas todo el oro? ]' 80
  promptalias 'box5'
endif
@setalias 'regbook1' 0x426c40f6
@setalias 'regbook2' 0x41b5cc68
@setalias 'bankbook' 0x45dec5ed
@removelist 'Runes1'
@removelist 'Runes2'
if not listexists 'Runes1'
  @createlist 'Runes1'
  pushlist 'Runes1' 17
  pushlist 'Runes1' 23
endif
if not listexists 'npcbodies'
  createlist 'npcbodies'
  pushlist 'npcbodies' 0x190
  pushlist 'npcbodies' 0x191
endif
@removelist 'NPC Names'
if not listexists 'NPCNames'
  createlist 'NPCNames'
  pushlist 'NPCNames' Blacksmith
endif
if not timerexists 'hourloop'
  createtimer 'hourloop'
endif
settimer 'hourloop' 0
clearignorelist
@clearjournal
/////////////////////////////////////////////
//CABIAR ACA RUNE BOOK/////
for 0 to 'Runes1'
  ///////////////////////////
  while mana < 20
  endwhile
  if @injournal 'world will save'
    pause 20000
    @clearjournal
  endif
  //CABIAR ACA RUNE BOOK/////
  useobject 'regbook1'
  waitforgump 0x554b87f3 5000
  sysmsg 'Book1' 33
  sysmsg 'Runes1[]' 33
  replygump 0x554b87f3 Runes1[]
  ///////////////////////////
  pause 2000
  for 0 to 'npcbodies'
    while @findtype npcbodies[] 'any' 'ground' 1 10
      waitforproperties 'found' 5000
      if not property 'guild' 'found' and not property 'instructor' 'found'
        if not property 'quest' 'found'
          for 0 to 'NPCNames'
            if property NPCNames[] 'found'
              // LAS COSAS
              waitforcontext 'found' 3 5000
              pause 1000
              //Consulto si tengo oro en el backpack, recaleo al banco
              replygump 0x9bade6ea 0
              playmacro 'MININGSHIELD SKY'
            endif
          endfor
        endif
      endif
      ignoreobject 'found'
    endwhile
  endfor
endfor
/////////////////////////////////////////////
