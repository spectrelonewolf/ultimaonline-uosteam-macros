//Atencion! esta macro no va con el 'LOOP' activo.
//Respetar los nombres de las 2 macros, esta se llama 'miasmaVolver' y la otra macro 'miasmaAtacar'
//En la linea 6 hay que colocar la posicion 'X' del mapa a donde se quiere regresar.
//En la linea 7 hay que colocar la posicion 'Y' del mapa a donde se quiere regresar.
//MIASMA x = 337  y = 1912
if not listexists 'position'
  createlist 'position'
  pushlist 'position' 337 //Posicion X a volver
  pushlist 'position' 1912  //Posicion Y a volver
endif
if not listexists 'unstuck'
  createlist 'unstuck'
  pushlist 'unstuck' 'east'
  pushlist 'unstuck' 'west'
  pushlist 'unstuck' 'south'
  pushlist 'unstuck' 'north'
endif
if list 'unstuck' == 0
  pushlist 'unstuck' 'east'
  pushlist 'unstuck' 'west'
  pushlist 'unstuck' 'south'
  pushlist 'unstuck' 'north'
endif
@settimer 'pathfind' 0
while not x == position[0]
  if @timer 'pathfind' >= 5000
    msg 'path cero'
    // Diverge
    if direction == 0
      @poplist 'unstuck' 'north'
    elseif direction == 2
      @poplist 'unstuck' 'east'
    elseif direction == 4
      @poplist 'unstuck' 'south'
    elseif direction == 6
      @poplist 'unstuck' 'west'
    endif
    // Unstuck
    if list 'unstuck' != 0
      for 5
        run 'unstuck[0]'
        pause 100
      endfor
      if list 'unstuck' == 0
        pushlist 'unstuck' 'east'
        pushlist 'unstuck' 'west'
        pushlist 'unstuck' 'south'
        pushlist 'unstuck' 'north'
      endif
    endif
  elseif x < position[0] and y < position[1]
    run 'southeast'
  elseif x > position[0] and y < position[1]
    run 'southwest'
  elseif x < position[0] and y > position[1]
    run 'northeast'
  elseif x > position[0] and y > position[1]
    run 'northwest'
  elseif x < position[0] and y == position[1]
    run 'east'
  elseif x > position[0] and y == position[1]
    run 'west'
  elseif x == position[0] and y < position[1]
    run 'south'
  elseif x == position[0] and y > position[1]
    run 'north'
  endif
  pause 100
endwhile
@settimer 'pathfind' 0
while not y == position[1]
  if @timer 'pathfind' >= 5000
    msg 'path cero'
    // Diverge
    if direction == 0
      @poplist 'unstuck' 'north'
    elseif direction == 2
      @poplist 'unstuck' 'east'
    elseif direction == 4
      @poplist 'unstuck' 'south'
    elseif direction == 6
      @poplist 'unstuck' 'west'
    endif
    // Unstuck
    if list 'unstuck' != 0
      for 5
        run 'unstuck[0]'
        pause 100
      endfor
      if list 'unstuck' == 0
        pushlist 'unstuck' 'east'
        pushlist 'unstuck' 'west'
        pushlist 'unstuck' 'south'
        pushlist 'unstuck' 'north'
      endif
    endif
  elseif x < position[0] and y < position[1]
    run 'southeast'
  elseif x > position[0] and y < position[1]
    run 'southwest'
  elseif x < position[0] and y > position[1]
    run 'northeast'
  elseif x > position[0] and y > position[1]
    run 'northwest'
  elseif x < position[0] and y == position[1]
    run 'east'
  elseif x > position[0] and y == position[1]
    run 'west'
  elseif x == position[0] and y < position[1]
    run 'south'
  elseif x == position[0] and y > position[1]
    run 'north'
  endif
  pause 100
endwhile
@playmacro 'miasmaAtacar'
