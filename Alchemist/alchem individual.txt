sysmsg '[SOLO ESCRIBE CUALQUIERA:]' 80
sysmsg 'AGPOT' 22
sysmsg 'STRPOT' 22
sysmsg 'CUREPOT' 22
sysmsg 'HEALPOT' 22
sysmsg 'RFRPOT' 22
sysmsg 'POIPOT' 22
sysmsg 'CONFPOT' 22
sysmsg 'EXPPOT' 22
headmsg 'Cual hago?'
pause 3000
//selecciono caja de guardado
if not @findalias 'box'
  headmsg 'Donde guardas todo?'
  promptalias 'box'
endif
if injournal 'AGPOT'
  clearjournal
  while not @dead 'self'
    while @movetype 0xf08 'backpack' 'box'
    endwhile
    while @movetype 0xf7b 'backpack' 'box'
    endwhile
    while @movetype 0xf0e 'backpack' 'box'
    endwhile
    if @counttype 0xf7b 0x0 'backpack' < 3
      movetype 0xf7b 'box' 'backpack' 0 0 0 0x0 300
      pause 1000
    endif
    if @counttype 0xf0e 0x0 'backpack' < 1
      movetype 0xf0e 'box' 'backpack' 0 0 0 0x0 100
      pause 1000
    endif
    if @counttype 0xe9b 0x0 'backpack' < 1
      movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
      pause 1000
    endif
    @usetype 0xe9b 0x0 'backpack'
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 8
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 9
    waitforgump 0x38920abd 15000
    for 0 to 100
      if injournal 'You have worn out your tool!' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
      replygump 0x38920abd 21
      waitforgump 0x38920abd 15000
      pause 100
      if injournal 'gump id not found' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
    endfor
  endwhile
elseif injournal 'STRPOT'
  clearjournal
  while not @dead 'self'
    //pots hechas
    while @movetype 0xf09 'backpack' 'box'
    endwhile
    //ingrediente
    while @movetype 0xf86 'backpack' 'box'
    endwhile
    //pots vacias
    while @movetype 0xf0e 'backpack' 'box'
    endwhile
    //ingrediente
    if @counttype 0xf86 0x0 'backpack' < 5
      movetype 0xf86 'box' 'backpack' 0 0 0 0x0 500
      pause 1000
    endif
    //pots vacias
    if @counttype 0xf0e 0x0 'backpack' < 1
      movetype 0xf0e 'box' 'backpack' 0 0 0 0x0 100
      pause 1000
    endif
    //morteros
    if @counttype 0xe9b 0x0 'backpack' < 1
      movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
      pause 1000
    endif
    //gump producto
    @usetype 0xe9b 0x0 'backpack'
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 29
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 9
    waitforgump 0x38920abd 15000
    //siempre lo mismo
    for 0 to 100
      if injournal 'You have worn out your tool!' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
      replygump 0x38920abd 21
      waitforgump 0x38920abd 15000
      pause 100
      if injournal 'gump id not found' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
    endfor
  endwhile
elseif injournal 'CUREPOT'
  clearjournal
  while not @dead 'self'
    //pots hechas
    while @movetype 0xf07 'backpack' 'box'
    endwhile
    //ingrediente
    while @movetype 0xf84 'backpack' 'box'
    endwhile
    //pots vacias
    while @movetype 0xf0e 'backpack' 'box'
    endwhile
    //ingrediente
    if @counttype 0xf84 0x0 'backpack' < 6
      movetype 0xf84 'box' 'backpack' 0 0 0 0x0 600
      pause 1000
    endif
    //pots vacias
    if @counttype 0xf0e 0x0 'backpack' < 1
      movetype 0xf0e 'box' 'backpack' 0 0 0 0x0 100
      pause 1000
    endif
    //morteros
    if @counttype 0xe9b 0x0 'backpack' < 1
      movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
      pause 1000
    endif
    //gump producto
    @usetype 0xe9b 0x0 'backpack'
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 43
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 16
    waitforgump 0x38920abd 15000
    //siempre lo mismo
    for 0 to 100
      if injournal 'You have worn out your tool!' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
      replygump 0x38920abd 21
      waitforgump 0x38920abd 15000
      pause 100
      if injournal 'gump id not found' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
    endfor
  endwhile
elseif injournal 'HEALPOT'
  clearjournal
  while not @dead 'self'
    //pots hechas
    while @movetype 0xf0c 'backpack' 'box'
    endwhile
    //ingrediente
    while @movetype 0xf85 'backpack' 'box'
    endwhile
    //pots vacias
    while @movetype 0xf0e 'backpack' 'box'
    endwhile
    //ingrediente
    if @counttype 0xf85 0x0 'backpack' < 7
      movetype 0xf85 'box' 'backpack' 0 0 0 0x0 700
      pause 1000
    endif
    //pots vacias
    if @counttype 0xf0e 0x0 'backpack' < 1
      movetype 0xf0e 'box' 'backpack' 0 0 0 0x0 100
      pause 1000
    endif
    //morteros
    if @counttype 0xe9b 0x0 'backpack' < 1
      movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
      pause 1000
    endif
    //gump producto
    @usetype 0xe9b 0x0 'backpack'
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 22
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 16
    waitforgump 0x38920abd 15000
    //siempre lo mismo
    for 0 to 100
      if injournal 'You have worn out your tool!' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
      replygump 0x38920abd 21
      waitforgump 0x38920abd 15000
      pause 100
      if injournal 'gump id not found' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
    endfor
  endwhile
elseif injournal 'RFRPOT'
  clearjournal
  while not @dead 'self'
    //pots hechas
    while @movetype 0xf0b 'backpack' 'box'
    endwhile
    //ingrediente
    while @movetype 0xf7a 'backpack' 'box'
    endwhile
    //pots vacias
    while @movetype 0xf0e 'backpack' 'box'
    endwhile
    //ingrediente
    if @counttype 0xf7a 0x0 'backpack' < 5
      movetype 0xf7a 'box' 'backpack' 0 0 0 0x0 500
      pause 1000
    endif
    //pots vacias
    if @counttype 0xf0e 0x0 'backpack' < 1
      movetype 0xf0e 'box' 'backpack' 0 0 0 0x0 100
      pause 1000
    endif
    //morteros
    if @counttype 0xe9b 0x0 'backpack' < 1
      movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
      pause 1000
    endif
    //gump producto
    @usetype 0xe9b 0x0 'backpack'
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 1
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 9
    waitforgump 0x38920abd 15000
    //siempre lo mismo
    for 0 to 100
      if injournal 'You have worn out your tool!' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
      replygump 0x38920abd 21
      waitforgump 0x38920abd 15000
      pause 100
      if injournal 'gump id not found' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
    endfor
  endwhile
elseif injournal 'POIPOT'
  clearjournal
  while not @dead 'self'
    //pots hechas
    while @movetype 0xf0a 'backpack' 'box'
    endwhile
    //ingrediente
    while @movetype 0xf88 'backpack' 'box'
    endwhile
    //pots vacias
    while @movetype 0xf0e 'backpack' 'box'
    endwhile
    //ingrediente
    if @counttype 0xf88 0x0 'backpack' < 8
      movetype 0xf88 'box' 'backpack' 0 0 0 0x0 800
      pause 1000
    endif
    //pots vacias
    if @counttype 0xf0e 0x0 'backpack' < 1
      movetype 0xf0e 'box' 'backpack' 0 0 0 0x0 100
      pause 1000
    endif
    //morteros
    if @counttype 0xe9b 0x0 'backpack' < 1
      movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
      pause 1000
    endif
    //gump producto
    @usetype 0xe9b 0x0 'backpack'
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 36
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 23
    waitforgump 0x38920abd 15000
    //siempre lo mismo
    for 0 to 100
      if injournal 'You have worn out your tool!' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
      replygump 0x38920abd 21
      waitforgump 0x38920abd 15000
      pause 100
      if injournal 'gump id not found' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
    endfor
  endwhile
elseif injournal 'CONFPOT'
  clearjournal
  while not @dead 'self'
    msg 'NO ESTA HECHA ESTA MACRO'
    //pots hechas
    while @movetype 0xf0a 'backpack' 'box'
    endwhile
    //ingrediente
    while @movetype 0xf88 'backpack' 'box'
    endwhile
    //pots vacias
    while @movetype 0xf0e 'backpack' 'box'
    endwhile
    //ingrediente
    if @counttype 0xf88 0x0 'backpack' < 8
      movetype 0xf88 'box' 'backpack' 0 0 0 0x0 800
      pause 1000
    endif
    //pots vacias
    if @counttype 0xf0e 0x0 'backpack' < 1
      movetype 0xf0e 'box' 'backpack' 0 0 0 0x0 100
      pause 1000
    endif
    //morteros
    if @counttype 0xe9b 0x0 'backpack' < 1
      movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
      pause 1000
    endif
    //gump producto
    @usetype 0xe9b 0x0 'backpack'
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 36
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 23
    waitforgump 0x38920abd 15000
    //siempre lo mismo
    for 0 to 100
      if injournal 'You have worn out your tool!' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
      replygump 0x38920abd 21
      waitforgump 0x38920abd 15000
      pause 100
      if injournal 'gump id not found' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
    endfor
  endwhile
elseif injournal 'EXPPOT'
  clearjournal
  while not @dead 'self'
    //pots hechas
    while @movetype 0xf0d 'backpack' 'box'
    endwhile
    //ingrediente
    while @movetype 0xf8c 'backpack' 'box'
    endwhile
    //pots vacias
    while @movetype 0xf0e 'backpack' 'box'
    endwhile
    //ingrediente
    if @counttype 0xf8c 0x0 'backpack' < 10
      movetype 0xf8c 'box' 'backpack' 0 0 0 0x0 1000
      pause 1000
    endif
    //pots vacias
    if @counttype 0xf0e 0x0 'backpack' < 1
      movetype 0xf0e 'box' 'backpack' 0 0 0 0x0 100
      pause 1000
    endif
    //morteros
    if @counttype 0xe9b 0x0 'backpack' < 1
      movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
      pause 1000
    endif
    //gump producto
    @usetype 0xe9b 0x0 'backpack'
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 50
    waitforgump 0x38920abd 15000
    replygump 0x38920abd 16
    waitforgump 0x38920abd 15000
    //siempre lo mismo
    for 0 to 100
      if injournal 'You have worn out your tool!' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
      replygump 0x38920abd 21
      waitforgump 0x38920abd 15000
      pause 100
      if injournal 'gump id not found' 'system'
        clearjournal
        movetype 0xe9b 'box' 'backpack' 0 0 0 0x0 1
        pause 500
        @usetype 0xe9b 0x0 'backpack'
      endif
    endfor
  endwhile
endif
